import json
#cargar el archivo creado anteriormente
def cargar_archivo(archivo):
    try:
        with open(archivo,"r") as datos:
            #habre el archivo y los datos son cargados 
            datos = json.load(datos)
            return datos
            #se crea un exepcion de error por si el archvo no fue encontrado 
    except FileNotFoundError:
            print("El Archivo no fue encontrado\n")
            quit()
#esta funcion busca el pais consultado por el usuario 
def consulta_pais(processed):
    
    vacio = None
    contenido = processed.get("country")
    #se pide que se ingrese el nombre que se desea buscar
    pais = str(input("Ingrese el pais: ")).capitalize()
    print(pais)
    print("\n")
    #se buscan los datos necesarios que coincidan con el pais buscado y se muestran al usuario 
    for row in contenido:
        if row.get(pais) != vacio:
            for row_2 in row.get(pais):
                print(f'Anio:{row_2.get("Anio")}')
                print(f'Mes:{row_2.get("Mes")}')
                print( f'Número absoluto de inmunizaciones: {row_2.get("total_vaccinations")}')
                print( f'Número total de personas vacunadas: {row_2.get("people_vaccinated")}')
                print( f'Número total de personas con el esquema completo: {row_2.get("people_fully_vaccinated")}')
                print(f'Vacuna utilizada: {row_2.get("Vacunas")}')
                print(f'Merma de vacunas: {row_2.get("Merma")}')
                print("---------------------------------------------------------")
    print("\n\n")            
#se le pide al usuario que ingrese la fecha de la cual desea saber los datos 
def consulta_pais_fecha(processed):
    vacio = None
    texto = "0"
    #se pide que ingrese primero el año y despues el mes 
    contenido = processed.get("fecha")
    Anio = int(input("Ingrese el año: "))
    Mes = str(input("Ingrese el mes: "))
    if len(Mes) < 2 :
        Mes = texto + Mes
        #despues se pide que ingrese el dia 
    Dia = str(input("Ingrese el Dia: "))
    if len(Dia) < 2 :
        Dia = texto + Dia
    Fecha = f'{Anio}-{Mes}-{Dia}'
    pais = str(input("Ingrese el pais: ")).capitalize()
    print("\n\n")
    print(Fecha)
    print(pais)
#se buscan los datos solicitados y se muestran los resultados al usuario 
    for row in contenido:
        if row.get(Fecha) != vacio:
            for row_2 in row.get(Fecha):
                if row_2.get(pais) != vacio:
                    for row_4 in row_2.get(pais):
                        if row_4 == "Anio":
                            print(f'Anio:{row_2.get(pais).get("Anio")}')
                        elif row_4 == "Mes":
                            print(f'Mes:{row_2.get(pais).get("Mes")}')
                        elif row_4 == "total_vaccinations":
                            print( f'Número absoluto de inmunizaciones: {row_2.get(pais).get("total_vaccinations")}')
                        elif row_4 == "people_vaccinated":
                            print( f'Número total de personas vacunadas: {row_2.get(pais).get("people_vaccinated")}')
                        elif row_4 == "people_fully_vaccinated":
                            print( f'Número total de personas con el esquema completo: {row_2.get(pais).get("people_fully_vaccinated")}')
                        elif row_4 == "Vacunas":
                            print(f'Vacuna utilizada: {row_2.get(pais).get("Vacunas")}')
                        elif row_4 == "Merma":
                            print(f'Merma de vacunas: {row_2.get(pais).get("Merma")}')
                    
                    print("---------------------------------------------------------")
    print("\n\n")
 #se le pide al usuario que ingrese la fecha que desea saber y el nombre de 2 paises diferentes 
 #para poder comparar los datos de estos paises en la misma fecha               
def consulta_pais_fecha_Comparacion(processed):
    vacio = None
    texto = "0"
    #se pide la fecha al usuario, primero el año y despues el mes 
    contenido = processed.get("fecha")
    Anio = str(input("Ingrese el año: "))
    Mes = str(input("Ingrese el mes: "))
    if len(Mes) < 2 :
        Mes = texto + Mes
        #se pide el dia al usuario
    Dia = str(input("Ingrese el Dia: "))
    if len(Dia) < 2 :
        Dia = texto + Dia
    Fecha = f'{Anio}-{Mes}-{Dia}'
    #y finalmente se le pide al usuario que ingrese el nombre del primer pais 
    pais = str(input("Ingrese el pais: ")).capitalize()
    #y que ingrese el nombre del segundo pais con el que lo desea comparar 
    pais_2 = str(input("Ingrese el pais: ")).capitalize()
    print("\n\n")
    print(Fecha)
    print(pais)
#se muestran los resultados de los datos solicitados del primer pais 
    for row in contenido:
        if row.get(Fecha) != vacio:
            for row_2 in row.get(Fecha):
                if row_2.get(pais) != vacio:
                    for row_4 in row_2.get(pais):
                        if row_4 == "Anio":
                            print(f'Anio:{row_2.get(pais).get("Anio")}')
                        elif row_4 == "Mes":
                            print(f'Mes:{row_2.get(pais).get("Mes")}')
                        elif row_4 == "total_vaccinations":
                            print( f'Número absoluto de inmunizaciones: {row_2.get(pais).get("total_vaccinations")}')
                        elif row_4 == "people_vaccinated":
                            print( f'Número total de personas vacunadas: {row_2.get(pais).get("people_vaccinated")}')
                        elif row_4 == "people_fully_vaccinated":
                            print( f'Número total de personas con el esquema completo: {row_2.get(pais).get("people_fully_vaccinated")}')
                        elif row_4 == "Vacunas":
                            print(f'Vacuna utilizada: {row_2.get(pais).get("Vacunas")}')
                        elif row_4 == "Merma":
                            print(f'Merma de vacunas: {row_2.get(pais).get("Merma")}')
                    
                    print("---------------------------------------------------------")

    print("*************************************************************************************************************")
    #se muestran los resultados de los datos solicitados del segundo pais 
    for row in contenido:
        if row.get(Fecha) != vacio:
            for row_2 in row.get(Fecha):
                if row_2.get(pais_2) != vacio:
                    for row_4 in row_2.get(pais_2):
                        if row_4 == "Anio":
                            print(f'Anio:{row_2.get(pais_2).get("Anio")}')
                        elif row_4 == "Mes":
                            print(f'Mes:{row_2.get(pais_2).get("Mes")}')
                        elif row_4 == "total_vaccinations":
                            print( f'Número absoluto de inmunizaciones: {row_2.get(pais_2).get("total_vaccinations")}')
                        elif row_4 == "people_vaccinated":
                            print( f'Número total de personas vacunadas: {row_2.get(pais_2).get("people_vaccinated")}')
                        elif row_4 == "people_fully_vaccinated":
                            print( f'Número total de personas con el esquema completo: {row_2.get(pais_2).get("people_fully_vaccinated")}')
                        elif row_4 == "Vacunas":
                            print(f'Vacuna utilizada: {row_2.get(pais_2).get("Vacunas")}')
                        elif row_4 == "Merma":
                            print(f'Merma de vacunas: {row_2.get(pais_2).get("Merma")}')
                    
                    print("---------------------------------------------------------")
    print("\n\n") 

#se llaman a las diferentes funciones para que el codigo las ejecute en el orden correcto y pueda realizas su tarea
def main():
    archivo = "Proyecto_2.json"
    processed = cargar_archivo(archivo)
    while True:
        #se muestran las opciones de busqueda al usuario
        print("\t*************************************")
        print("\t*          Consultas Json           *")
        print("\t*************************************")
        print("1. Consulta Pais")
        print("2.Consulta Pais y fecha")
        print("3.Comparacion de dos paises")
        print("0. Salir")
        eleccion = int(input("--> "))
        #si selecciona la opcion 1 se llama a la funcion consulta_pais
        if eleccion == 1:
            print("-----------------------------------------------------------")
            consulta_pais(processed)
        #si selecciona la opcion 2 se llama a la funcion consulta_pais_fecha
        elif eleccion == 2:
            print("-----------------------------------------------------------")
            consulta_pais_fecha(processed)
        #si selecciona la opcion 3 se llama a la funcion consulta_pais_fecha_comparacion    
        elif eleccion == 3:
            print("-----------------------------------------------------------")
            consulta_pais_fecha_Comparacion(processed)
        else:
            quit()

if __name__ == "__main__":
    main()
