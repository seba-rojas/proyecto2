import json
#se importa la carpeta json para poder realizar el trabajo 
#esa funcion abre los dos archivos y guarda los datos de estos archivos en la variable datos y datos_2
def cargar_archivos(archivo = None, archivo_2 = None):

    try:   
        with open(archivo) as archivo_csv, open(archivo_2) as archivo_2_csv:
            datos = archivo_csv.readlines()
            datos_2 = archivo_2_csv.readlines()
            return datos,datos_2
            #se crea un axepcion por si el archivo no se encuentra asi el codigo pueda seguir funcionando
    except FileNotFoundError:
        print("Error, el archivo no fue encontrado")
        quit()
#crea el diccionario de los paises y crea una lista de este mismo y almacena cada pais en una casilla, de tal forma que no se repita.
def estructura_1(datos,datos_2):
    i = 0
    processed = {}

    paises = {}
    processed["country"] = []
    for linea in datos:
        linea = linea.rstrip('\n')
        aux = linea.split(",")
        if i == 0:
            categorias = aux
            i+= 1
            pass
        elif aux[0] not in paises:
        #si aux[0] no esta en paises entonces se crea una lista y esta es agregada al diccionario processed
            paises[aux[0]] = []
    processed["country"].append(paises) 
    #se retorna el discionario processed y paises, junto con la lista categorias 
    return processed,categorias,paises
#crea el diccionario de las fechas y crea una lista de este mismo y almacena cada fecha en una casilla, de tal forma que no se repita.
def estructura_2(datos,datos_2,processed):
    processed["fecha"] = []
    i = 0
    Fechas = {}
    for linea in datos:
        linea = linea.rstrip('\n')
        aux = linea.split(",")
        aux_2 = aux[2]
        if i == 0:
            categorias_2 = aux
            i+= 1
            pass
#si aux_2 no esta en fecha estas son agregadas a la lista 
        elif aux_2 not in Fechas:
            Fechas[aux_2] = []
    processed["fecha"].append(Fechas)
    return processed,categorias_2,Fechas

#crea una matriz de cada archivo 
def matriz_archivos(datos,datos_2,processed,categorias):
    
    matriz_archivo = []
    matriz_archivo_2 = []
    i = 0
    j = 0
    for linea in datos:
    #cada casilla de las listas son separados por una coma 
        linea_lista = linea.split(",")
        if i == 0:
        #i igual a 0 para que no tome el primer dato si no que empieze de la segunda linea
            largo = len(linea_lista)-1
            i += 1
            pass
        elif i != 0:
        #es distento de 0 para que no tome el encabezado de los datos 
            matriz_archivo.append(linea_lista)
#se crea un lista para guardar los paises del segundo archivo 
    paises_3 = []
#linea_2 recorre los datos uno por uno de datos_2
    for linea_2 in datos_2:
        linea_2 = linea_2.rstrip('\n')
        linea_lista_2 = linea_2.split(",")
        if j == 0:
            largo_2 = len(linea_lista_2)-1
            j += 1
            pass
        elif j != 0:
            matriz_archivo_2.append(linea_lista_2)
            aux2 = linea_lista_2[0]
        #si aux2 no esta en paises_3 este es agregado a la lista
            if aux2 not in paises_3:
                paises_3.append(aux2)

    
    return matriz_archivo,matriz_archivo_2,paises_3
#esta funcion campara las fechas contenidas en el archivo 1 
def comparacion_fechas_1(datos,datos_2,processed,categorias,matriz_archivo,paises_2_lista,matriz_archivo_2,paises_3):
    i = 0
    j = 0
    texto_temp = ""
    for fila in range(len(matriz_archivo)):
#fila recorre matris_archivo 
#si i es menor a 2 entonces se le suma uno y pasa
        if i < 2:
            i += 1
            pass
#si i es mayor o igual a 2 las fechas son agregadas separadas por año, mes y dia
        elif i >= 2:
            fecha_1 = matriz_archivo[fila-1][2]
            fecha_2 = matriz_archivo[fila][2]
            fecha_1_Nueva = fecha_1.split("-")
            fecha_2_Nueva = fecha_2.split("-")
#si los meses son diferentes se crea otra lista la cual es agregada a la fila principal 
            if fecha_1_Nueva[1] != fecha_2_Nueva[1]:
                fila_principal = matriz_archivo[fila-1]
                agregar_contenido_1(datos,datos_2,processed,categorias,matriz_archivo,matriz_archivo_2,fila_principal,paises_2_lista,fecha_1_Nueva,paises_3)
#esta funcion campara las fechas contenidas en el archivo 2
def comparacion_fechas_2(datos,datos_2,processed,categorias,matriz_archivo,paises_2_lista,matriz_archivo_2,paises_3):
    texto = ""
    suma = 0
    
#fila recorre matris_archivo 
    for fila_2 in range(len(matriz_archivo_2)):
#si i es menor a 2 entonces pasa
        if fila_2 < 2:
            pass
#si i es mayor o igual a 2 las fechas son agregadas separadas por año, mes y dia
        elif fila_2 >= 2:
            fecha_1 = matriz_archivo_2[fila_2-1][1]
            fecha_2 = matriz_archivo_2[fila_2][1]
            fecha_1_2 = fecha_1.split("-")
            fecha_2_2 = fecha_2.split("-")
#si el mes y el día de la linea anterior y el actual son iguales suma las vacunas y su correspondiente cantidad de vacunas
            if (fecha_1_2[1] == fecha_2_2[1]) and (fecha_1_2[2] == fecha_2_2[2]):
                texto = texto + f' {matriz_archivo_2[fila_2][2]}({matriz_archivo_2[fila_2][3]})'
                suma += int(float(matriz_archivo_2[fila_2][3]))
#si el mes de la linea anterior es igual a la linea actual y el dia anterior es menor al dia actual 
            elif (fecha_1_2[1] == fecha_2_2[1]) and (fecha_1_2[2] < fecha_2_2[2]):
#texto se reinicia y se le agregan los nuevos datos     
                texto = ""
                suma = 0
                texto = texto + f' {matriz_archivo_2[fila_2][2]}({matriz_archivo_2[fila_2][3]})'
                suma += int(float(matriz_archivo_2[fila_2][3]))
#si el mes anterior es menor al mes actual y los años son iguales 
            elif (fecha_1_2[1] <  fecha_2_2[1]) and (fecha_1_2[0] == fecha_1_2[0]) :
                pais_presente = matriz_archivo_2[fila_2-1]
                fecha_2_3 = matriz_archivo_2[fila_2-1][1]
                fecha_2_3 = fecha_2_3.split("-")
                agregar_contenido_2(datos,datos_2,processed,categorias,matriz_archivo,matriz_archivo_2,paises_2_lista,paises_3,texto,pais_presente,suma,fecha_2_3)
#si el mes anterior es mañor al mes actual y los años son iguales 
            elif (fecha_1_2[1] >  fecha_2_2[1]) and (fecha_1_2[0] == fecha_1_2[0]) :
                pais_presente = matriz_archivo_2[fila_2-1]
                fecha_2_3 = matriz_archivo_2[fila_2-1][1]
                fecha_2_3 = fecha_2_3.split("-")
                agregar_contenido_2(datos,datos_2,processed,categorias,matriz_archivo,matriz_archivo_2,paises_2_lista,paises_3,texto,pais_presente,suma,fecha_2_3)

        



#esta funcion agrega los contenidos recopilados en el orden correcto
def agregar_contenido_1(datos,datos_2,processed,categorias,matriz_archivo,matriz_archivo_2,fila_principal,paises_2_lista,fecha_1_Nueva,paises_3):
    processed_2 = processed
    paises_diccionario = processed_2.get("country")
    i = 0
    #cuando hay un espacio vacio se le agrega un 0.0
    if not fila_principal[3]:
        fila_principal[3] = 0.0
    if not fila_principal[4]:
        fila_principal[4] = 0.0
    if not fila_principal[5]:
        fila_principal[5] = 0.0
        
    

    for row in paises_diccionario:
        for row_2 in paises_2_lista:
#si row_2 no esta en paises_3 el dato es tranformado en una variable float y despues a variable entero
#y es guardado en la variable correspondiente.
            if fila_principal[0] == row_2 and row_2 not in paises_3:
                Total_number_of_vaccinations = int(float(fila_principal[3]))
                Total_number_of_people_vaccinated =int(float(fila_principal[4]))
                Total_number_of_people_fully_vaccinated = int(float(fila_principal[5]))
                diccionario_propio = {
#se ordena la manera en que se quiere mostrar la informacion
#primero se muestra el año, despues el mes.


                    "Anio":fecha_1_Nueva[0],
                    "Mes":fecha_1_Nueva[1],
#despues se muestra el total de vacunas despues el total de personas vacunadas
                    categorias[3]:  Total_number_of_vaccinations,
                    categorias[4]: Total_number_of_people_vaccinated,
                    categorias[5]: Total_number_of_people_fully_vaccinated,
                    "vacunas":0,
#y finalmente se muestra la merma (vacunas perdidas)
                    "Merma":0
                   }
                row[row_2].append(diccionario_propio)
#esta funcion agrega los contenidos del archivo 2 recopilados en el orden correcto             
def agregar_contenido_2(datos,datos_2,processed,categorias,matriz_archivo,matriz_archivo_2,paises_2_lista,paises_3,texto,pais_presente,suma,fecha_2_3):
    processed_2 = processed
    paises_diccionario = processed_2.get("country")
    i = 0
#cumpla la misma funcion que el agregar_contenido_1 pero con el archivo 2    

    for row in paises_diccionario:
        for row_2 in paises_3:
            if  row_2 == pais_presente[0]:
             #cuando hay un espacio vacio se le agrega un 0.0
                for row_3 in range(len(matriz_archivo)):
                    if not matriz_archivo[row_3-1][3]:
                        matriz_archivo[row_3-1][3] = 0.0
                    if not matriz_archivo[row_3-1][4]:
                        matriz_archivo[row_3-1][4] = 0.0
                    if not matriz_archivo[row_3-1][5]:
                        matriz_archivo[row_3-1][5] = 0.0
                    fila_prin = matriz_archivo[row_3-1][0]
                    if row_2 == fila_prin:
#si row_2 es igual a fila_prin se agregan la fecha anterior y actual y son separados por guiones

                        fecha_prin = matriz_archivo[row_3-1][2]
                        fecha_prin_2 = matriz_archivo[row_3][2]
                        fecha_prin = fecha_prin.split("-")
                        fecha_prin_2 = fecha_prin_2.split("-")
#si el mes es igual al mes del archivo 2 llamado en la funcion anterior y el mes es diferente al mes del primer archivo entonces 
#los datos son tranformados en una variable float y despues a variable entero
#y es guardado en la variable correspondiente.
                        if  fecha_prin[1] == fecha_2_3[1] and fecha_prin[1] != fecha_prin_2[1]:
                            Total_number_of_vaccinations = int(float(matriz_archivo[row_3-1][3]))
                            Total_number_of_people_vaccinated = int(float(matriz_archivo[row_3-1][4]))
                            Total_number_of_people_fully_vaccinated = int(float(matriz_archivo[row_3-1][5]))
                            diccionario_propio = {
#se ordena la manera en que se quiere mostrar la informacion
#primero se muestra el año, despues el mes.
                                "Anio":fecha_prin[0],
                                "Mes":fecha_prin[1],
#despues se muestra el total de vacunas despues el total de personas vacunadas                             
                                categorias[3]: Total_number_of_vaccinations,
                                categorias[4]: Total_number_of_people_vaccinated,
                                categorias[5]: Total_number_of_people_fully_vaccinated,
#tambien se muestra el tipo de vacuna y su respectiva cantidad                              
                                "Vacunas":texto,
#y finalmente se muestra la merma (vacunas perdidas)                              
                                "Merma":(suma-int(float(matriz_archivo[row_3-1][3])))
                            }
                            
                            row[row_2].append(diccionario_propio)
                            break



#esta funcion agrega la estructura de las fechas 
def agregar_contenido_3(datos,datos_2,processed,categorias,matriz_archivo,paises_2_lista,matriz_archivo_2,paises_3,Fechas_lista):
    
    fechas_diccionario = processed.get("fecha")
    print(fechas_diccionario)
    for row in fechas_diccionario:
        for row_2 in Fechas_lista:
            for fila in range(len(matriz_archivo)):
                if row_2 == matriz_archivo[fila-1][2]:
                    #cuando hay un espacio vacio se le agrega 0.0
                    if not  matriz_archivo[fila-1][3]:
                        matriz_archivo[fila-1][3] = 0.0
                    if not matriz_archivo[fila-1][4]:
                        matriz_archivo[fila-1][4] = 0.0
                    if not matriz_archivo[fila-1][5]:
                        matriz_archivo[fila-1][5] = 0.0
                        #cada dato es separado por un guion 
                    fecha_respectiva = matriz_archivo[fila-1][2].split("-")
                    diccionario_propio_dos = {matriz_archivo[fila-1][0]:{"Anio":fecha_respectiva[0],"Mes":fecha_respectiva[1],categorias[3]: int(float(matriz_archivo[fila-1][3])),categorias[4]: int(float(matriz_archivo[fila-1][3])),categorias[5]: int(float(matriz_archivo[fila-1][3])),"Vacunas":0,"Merma":0}}
                    row[row_2].append(diccionario_propio_dos)

    for row in fechas_diccionario:
        for row_2 in Fechas_lista:
            for fila in range(len(matriz_archivo)):
                if row_2 == matriz_archivo[fila-1][2]:
                    #cuando hay un espacio vacio se le agrega 0.0
                    if not  matriz_archivo[fila-1][3]:
                        matriz_archivo[fila-1][3] = 0.0
                    if not matriz_archivo[fila-1][4]:
                        matriz_archivo[fila-1][4] = 0.0
                    if not matriz_archivo[fila-1][5]:
                        matriz_archivo[fila-1][5] = 0.0
                         #cada dato es separado por un guion 
                    fecha_respectiva = matriz_archivo[fila-1][2].split("-")
                    diccionario_propio_dos = {matriz_archivo[fila-1][0]:{"Anio":fecha_respectiva[0],"Mes":fecha_respectiva[1],categorias[3]: int(float(matriz_archivo[fila-1][3])),categorias[4]: int(float(matriz_archivo[fila-1][3])),categorias[5]: int(float(matriz_archivo[fila-1][3])),"Vacunas":0,"Merma":0}}
                    row[row_2].append(diccionario_propio_dos)



def main():
#se llaman a las diferentes funciones para que el codigo las ejecute en el orden correcto y pueda realizas su tarea
    archivo = "country_vaccinations.csv"
    archivo_2 = "country_vaccinations_by_manufacturer.csv"
    datos,datos_2 = cargar_archivos(archivo,archivo_2)

    processed,categorias,paises = estructura_1(datos,datos_2)
    paises_2_lista = list(paises.keys())
    matriz_archivo,matriz_archivo_2,paises_3 = matriz_archivos(datos,datos_2,processed,categorias)
    comparacion_fechas_1(datos,datos_2,processed,categorias,matriz_archivo,paises_2_lista,matriz_archivo_2,paises_3)
    comparacion_fechas_2(datos,datos_2,processed,categorias,matriz_archivo,paises_2_lista,matriz_archivo_2,paises_3)
    processed,categorias,Fechas = estructura_2(datos,datos_2,processed)
    Fechas_lista = list(Fechas.keys())
    agregar_contenido_3(datos,datos_2,processed,categorias,matriz_archivo,paises_2_lista,matriz_archivo_2,paises_3,Fechas)

    
    with open("Proyecto_2.json","w") as file:
        ejemplo = json.dump(processed,file,indent=4)
        file.write(ejemplo)
        file.close()
